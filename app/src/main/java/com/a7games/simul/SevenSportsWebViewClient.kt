package com.a7games.simul

import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient

class SevenSportsWebViewClient(
    private val sevenActions: (String) -> Unit
) : WebViewClient() {
    override fun shouldOverrideUrlLoading(
        sevenSevenView: WebView?,
        sevRequest: WebResourceRequest?
    ): Boolean {
        val sevReq = sevRequest?.url
        return if (sevReq != null) {
            sevenActions(sevReq.toString())
            true
        }
        else {
            super.shouldOverrideUrlLoading(sevenSevenView, sevRequest)
        }
    }
}
