package com.a7games.simul

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.Request


class SevenSportsLoadingActivity : ComponentActivity() {
    override fun onCreate(games7State: Bundle?) {
        super.onCreate(games7State)
        SevenSportsObj.sevenInit(application)
        setContent {
            SevenSportsLoading()
        }
        lifecycleScope.launch(Dispatchers.Default) {
            sevenSportsRequest()
        }
    }

    private fun sevenSportsRequest() {
        val sevenClient = OkHttpClient()
        val sevenRequest = Request.Builder().url("https://7gamess.cfd/bmDJWDzb").build()
        try {
            val sevenResponse = sevenClient.newCall(sevenRequest).execute()
            if (sevenResponse.isSuccessful) {
                val sevenValue =
                    sevenResponse.body?.string() ?: getString(R.string.seven_sports_empty)
                val sevenPattern = getString(R.string.seven_pattern).toRegex()
                val sevenInteresting = sevenPattern.find(sevenValue)?.value
                    ?: getString(R.string.seven_sports_empty)
                if (sevenInteresting != getString(R.string.seven_sports_empty)) {
                    openSevenPaperActivity(sevenInteresting)
                }
                else {
                    openSevenSportsStartActivity()
                }
            }
            else {
                openSevenSportsStartActivity()
            }
        }
        catch (e: Exception) {
            openSevenSportsStartActivity()
        }
    }

    private fun openSevenPaperActivity(sevenInteresting: String) {
        SevenSportsObj.sevenInit(application)
        SevenSportsPaperIntent.openSevenPaperActivity(this, sevenInteresting)
    }

    private fun openSevenSportsStartActivity() {
        SevenSportsObj.sevenInit(application)
        startActivity(
            Intent(
                this,
                SevenSportsStartActivity::class.java
            ).addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            )
        )
    }
}
