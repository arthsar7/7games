package com.a7games.simul

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Preview(showBackground = true)
@Composable
fun SevenPostCard(
    sevenPost: SevenSportsPost = SevenSportsInfoObj.sevenSportsPostList[7],
    onSevenBackClick: () -> Unit = {}
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(color = colorResource(id = R.color.sevenGreen))
    ) {
        Image(
            painter = painterResource(id = R.drawable.sevensports_back),
            contentDescription = null,
            modifier = Modifier
                .fillMaxSize()

        )
        Image(
            painter = painterResource(id = R.drawable.sportseven_back_btn),
            contentDescription = null,
            modifier = Modifier
                .align(Alignment.TopStart)
                .size(60.dp)
                .padding(start = 24.dp, top = 16.dp)
                .clickable {
                    onSevenBackClick()
                }
        )
        Card(
            modifier = Modifier
                .fillMaxSize()
                .padding(start = 20.dp, end = 20.dp, top = 64.dp, bottom = 24.dp)
                .align(Alignment.Center)
                .verticalScroll(rememberScrollState()),
            colors = CardDefaults.cardColors(containerColor = Color.White)
        ) {
            Column {
                Image(
                    painterResource(id = sevenPost.sevenImageId),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(123.dp)
                )
                Text(
                    text = sevenPost.sevenTitle,
                    modifier = Modifier
                        .padding(16.dp),
                    fontSize = 20.sp,
                    fontFamily = FontFamily(Font(R.font.seven_kodchasan_bold)),
                    lineHeight = 24.sp,
                    color = Color.Black
                )
                Text(
                    text = sevenPost.sevenInfo,
                    modifier = Modifier.padding(start = 16.dp, end = 16.dp),
                    fontSize = 14.sp,
                    fontFamily = FontFamily(Font(R.font.seven_kodchasan)),
                    fontWeight = FontWeight.SemiBold,
                    color = colorResource(id = R.color.sevenGray)
                )
            }
        }
    }
}