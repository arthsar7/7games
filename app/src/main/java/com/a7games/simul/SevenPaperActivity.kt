package com.a7games.simul

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.webkit.CookieManager
import android.webkit.ValueCallback
import android.webkit.WebSettings
import android.webkit.WebView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.a7games.simul.databinding.ActivitySevenPaperActivtiyBinding
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date

class SevenPaperActivity : AppCompatActivity() {
    private val sevenFunStartForResult = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) {
        if (it.resultCode == RESULT_OK) {
            val sevenFilePath = it.data?.data ?: sevenImageUri ?: return@registerForActivityResult
            sevenSportsFilePathCallback?.onReceiveValue(arrayOf(sevenFilePath))
            sevenSportsFilePathCallback = null
        }
        else {
            sevenSportsFilePathCallback?.onReceiveValue(null)
        }
    }
    private val sevenPaperBinding by lazy {
        ActivitySevenPaperActivtiyBinding.inflate(layoutInflater)
    }
    private val sevenInfo by lazy {
        intent?.getStringExtra(SevenSportsPaperIntent.sevenInteresting)
            ?: getString(R.string.seven_sports_empty)
    }
    private var sevenSportsFilePathCallback: ValueCallback<Array<Uri>>? = null
    private var sevenImageUri: Uri? = null

    override fun onCreate(seven7Bundle: Bundle?) {
        super.onCreate(seven7Bundle)
        SevenSportsObj.sevenInit(application)
        setContentView(sevenPaperBinding.root)
        with(sevenPaperBinding.sevenSportsPaper) {
            settings.setSevenSettings()
            setSevenWebViewClient()
            handleSevenBack()
            setSevenWebChromeClient()
        }
        val sevenPaperCookie = CookieManager.getInstance()
        sevenPaperCookie.setAcceptCookie(true)
        if (seven7Bundle == null) {
            sevenPaperBinding.sevenSportsPaper.loadUrl(sevenInfo)
        }
        else {
            sevenPaperBinding.sevenSportsPaper.restoreState(seven7Bundle)
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun WebSettings.setSevenSettings() {
        setSupportMultipleWindows(true)
        javaScriptEnabled = true
        loadWithOverviewMode = true
        builtInZoomControls = true
        setSupportZoom(false)
        displayZoomControls = true
        useWideViewPort = true
        domStorageEnabled = true
        databaseEnabled = true
        allowFileAccess = true
        allowContentAccess = true
    }

    private fun WebView.setSevenWebViewClient() {
        webViewClient = SevenSportsWebViewClient {
            sevenPaperBinding.sevenSportsPaper.loadUrl(it)
        }
    }
    private fun WebView.handleSevenBack() {
        if (canGoBack()) {
            goBack()
        }
        else {
            notGoBack()
        }
    }

    private fun notGoBack() {
        SevenSportsObj.sevenInit(application)
    }
    private fun WebView.setSevenWebChromeClient() {
        webChromeClient = SevenSportsWebChromeClient(
            sevenOnCreateInfo = {
                sevenPaperBinding.sevenSportsPaper.loadUrl(it)
            },
            onShowFileSevenAction = {
                sevenSportsFilePathCallback = null
                sevenSportsFilePathCallback = it
                sevenSportSomePhoto()
            }
        )

    }

    private fun sevenSportSomePhoto() {
        val sevenSportsPhotoFile: File?
        val sevenSportsAuthorities: String = this.packageName + ".provider"
        try {
            sevenSportsPhotoFile = sevenSportsCreateImageFile()
            sevenImageUri = FileProvider
                .getUriForFile(this, sevenSportsAuthorities, sevenSportsPhotoFile)
        }
        catch (e: IOException) {
            e.printStackTrace()
        }
        val natanChooserIntent = Intent(Intent.ACTION_GET_CONTENT)
        natanChooserIntent.addCategory(Intent.CATEGORY_OPENABLE)
        natanChooserIntent.type = "image/*"
        sevenFunStartForResult.launch(natanChooserIntent)
    }

    @SuppressLint("SimpleDateFormat")
    private fun sevenSportsCreateImageFile(): File {
        val prikolDir = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES
            ), getString(R.string.sevenandroidfolder)
        )
        if (!prikolDir.exists()) {
            prikolDir.mkdirs()
        }

        val sevenInfoUrlImageFileName =
            getString(R.string.sevenjpeg) + SimpleDateFormat(getString(R.string.sevenpattern)).format(
                Date()
            )
        return File(
            prikolDir,
            File.separator + sevenInfoUrlImageFileName + getString(R.string.seven_jpg)
        )
    }
}

