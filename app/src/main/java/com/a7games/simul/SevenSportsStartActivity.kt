package com.a7games.simul

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent


class SevenSportsStartActivity : ComponentActivity() {
    override fun onCreate(sevenSportsBundle: Bundle?) {
        super.onCreate(sevenSportsBundle)
        SevenSportsObj.sevenInit(application)
        setContent {
            SevenSportsMenu {
                startActivity(
                    Intent(
                        this,
                        SevenSportsInfoActivity::class.java
                    )
                )
            }
        }
    }
}
