package com.a7games.simul

import android.app.Application

object SevenSportsObj {
    private var sevenPackageName: String? = null
    fun sevenInit(sevenApplication: Application) {
        if (sevenApplication.packageName.isNullOrBlank()) {
            val sevenWarning = sevenApplication
                .getString(R.string.sevencontext_packagename_is_null_or_blank)
            throw Exception(sevenWarning)
        }
        else if (sevenApplication.packageName.isNotBlank()) {
                sevenPackageName = sevenApplication.packageName
            }
        }
}