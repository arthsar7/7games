package com.a7games.simul

import android.content.Context
import android.content.Intent

object SevenSportsPaperIntent {
    const val sevenInteresting = "sevenInteresting"
    fun openSevenPaperActivity(sevenContext: Context, sevenInteresting: String) {
        val sevenIntent = Intent(sevenContext, SevenPaperActivity::class.java)
        sevenIntent.putExtra(this.sevenInteresting, sevenInteresting)
        val sevenFlags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        sevenIntent.addFlags(sevenFlags)
        sevenContext.startActivity(sevenIntent)
    }
}