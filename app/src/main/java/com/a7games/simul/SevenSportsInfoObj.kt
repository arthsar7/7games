package com.a7games.simul

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.Calendar

object SevenSportsInfoObj {
    val sevenSportsPostList = mutableListOf<SevenSportsPost>()
    init {
        initSevenSportsPost()
    }
    @SuppressLint("SimpleDateFormat")
    private fun initSevenSportsPost() {
        val sevenDate = Calendar.getInstance().time
        val sevenFormatter = SimpleDateFormat("dd.MM.yyyy")
        val sevenDateFormatted = sevenFormatter.format(sevenDate)
        sevenSportsPostList.addAll(
            listOf(
                SevenSportsPost(
                    sevenTitle = "Maria Sakkari's impressive comeback, Jack Draper's not-so-impressive singing, and more",
                    sevenDate = sevenDateFormatted,
                    sevenInfo = "After losing in the first round of the US Open at the end of August, a despondent Maria Sakkari told reporters that she was considering taking a break from the sport. It was her third straight opening-round loss at a major, and she tearfully said she was \"uncertain\" about what was next.\n" +
                            "On Saturday, less than a month later, Sakkari won the title at the 1000-level Guadalajara Open.\n" +
                            "Sakkari didn't drop a set during her run to the title in Mexico -- and her 7-5, 6-3 victory over Caroline Dolehide in the final resulted in the biggest title of her career and her first since 2019. She fell to the ground after the match was over and couldn't hide just how much the moment meant to her.\n" +
                            "\n" +
                            "\"I'm just very proud of myself,\" Sakkari, 28, told WTA Insider after the match. \"Being like that at the US Open, wanting to take a break, and then finding the joy again during the week. It's a big title, my biggest title, it means so much.\n" +
                            "\"But over everything, it's just pure happiness and pure joy.\"",
                    sevenImageId = R.drawable.sevensports_maria
                ),
                SevenSportsPost(
                    sevenTitle = "Novak Djokovic's players' association gains momentum three years after creation",
                    sevenDate = sevenDateFormatted,
                    sevenInfo = "It began with almost no fanfare.\n" +
                            "Before play got underway at the spectator-less, pandemic-restricted 2020 US Open, No. 1-ranked Novak Djokovic and fellow ATP player Vasek Pospisil announced the creation of the Professional Tennis Players Association (PTPA), first with a letter to their ATP peers, and then with a photo, posted by Pospisil to social media, featuring several dozens of players who supported the initiative.\n" +
                            "\n" +
                            "The organization was not a union, as players are independent contractors and not ATP employees, but its central mission to \"promote, protect and represent the interests of its players\" made it seem similar to one.\n" +
                            "Few other details were provided at the time, and the lack of involvement from women players drew strong and swift criticism. Ultimately, the announcement left more questions than answers about the PTPA.\n" +
                            "But three years later, Djokovic and Pospisil are prouder than ever of the state of their once-fledgling organization.",
                    sevenImageId = R.drawable.sevensports_vasek
                ),
                SevenSportsPost(
                    sevenTitle = "Fifty years after Battle of the Sexes, gender equity in tennis remains elusive",
                    sevenDate = sevenDateFormatted,
                    sevenInfo = "\"Thank you Billie, for fighting for this,\" she said as she looked towards Billie Jean King.\n" +
                            "At just 19, Gauff doesn't know a US Open without equal prize money between the men and women. Her paycheck was the same amount as the one that men's champion Novak Djokovic received.\n" +
                            "This year marked 50 years of equal pay at the tournament, commemorated with tributes to King -- the groundbreaking pioneer who pushed to make it happen after founding the Women's Tennis Association (WTA) earlier that same year.\n" +
                            "EDITOR'S PICKS\n" +
                            "∘Bill eyes Congress honor for tennis legend King\n" +
                            "∘12d\n" +
                            "∘OTL: The Match Maker\n" +
                            "∘13dDon Van Natta Jr.\n" +
                            "∘Mixed reactions on Match Maker\n" +
                            "∘10yDon Van Natta Jr. and William Weinbaum\n" +
                            "The US Open became the first sporting event in the world to offer equal purses for its male and female competitors in 1973, and since then, the three remaining Grand Slam events followed suit, starting with the Australian Open",
                    sevenImageId = R.drawable.sevensports_bill
                ),
                SevenSportsPost(
                    sevenTitle = "Ranking Novak Djokovic's 24 Grand Slam tennis titles",
                    sevenDate = sevenDateFormatted,
                    sevenInfo = "In June 2003, Novak Djokovic secured his first professional title, beating Cesar Ferrer Victoria to win a small tournament in Serbia. In 2004, he earned his first win over a top-100 opponent. In 2005, he earned his first win over a top-10 opponent. In 2006, he reached his first Slam quarterfinal, followed by his first Slam final -- and his first wins over Rafael Nadal and Roger Federer -- in 2007 and first Slam title in 2008. The accomplishments were slow but steady. And they've never really stopped.\n" +
                            "EDITOR'S PICKS\n" +
                            "∘Djokovic's Grand Slam record is the latest step to becoming the greatest\n" +
                            "∘21dD'Arcy Maine\n" +
                            "∘A Nadal comeback? US Open vindication for Sabalenka? We predict the 2024 tennis major winners\n" +
                            "∘21dD'Arcy Maine\n" +
                            "∘Highlights from an unpredictable US Open\n" +
                            "∘22dAlyssa Roenigk\n" +
                            "In the 15 years or so since his first Slam title, Djokovic has won 23 more. He secured No. 24 on Sunday evening in New York, defeating Daniil Medvedev in three grueling sets and briefly breaking down in tears after hugging his 6-year-old daughter, who had served as a vocal front-row cheerleader during",
                    sevenImageId = R.drawable.sevensports_djokovic
                ),
                SevenSportsPost(
                    sevenTitle = "Chronicling Coco Gauff's week as US Open champion",
                    sevenDate = sevenDateFormatted,
                    sevenInfo = "The 19-year-old American won the US Open on Saturday, her first ever Grand Slam victory. She became the youngest American to win a major tournament since Serena Williams won the US Open in 1999 at 17.\n" +
                            "From a phone call with President Joe Biden to another primetime showing in New York, here's a look inside Gauff's whirlwind week since becoming champ:\n" +
                            "Saturday: Gauff thanks New York crowd\n" +
                            "Since her US Open debut in 2019, Gauff's been the crowd favorite in the Big Apple. It became even more evident during her championship run this year, the fans showing their support throughout each match.\n" +
                            "Gauff paid them back moments before lifting the trophy, showing how much the fans in New York meant to her.\n" +
                            "\"Last but not least, thank you to New York,\" she said. \"Thank you to you guys. You guys pulled me through this gas fire. The supporters that I have mean so much to me, so thank you all. And thank you to everyone who made this tournament possible.\"\n" +
                            "Saturday: Presidential congratulations\n" +
                            "The 46th President offered his congratulations on X, formerly known as Twitter, but also made a phone call to the teenager.",
                    sevenImageId = R.drawable.sevensports_coco
                ),
                SevenSportsPost(
                    sevenTitle =  "Victorious emotions of Nadiya Kichenok and German Fridzam",
                    sevenDate = sevenDateFormatted,
                    sevenInfo = "On Sunday, September 17, the last matches of the 2023 Davis Cup group stage took place. Based on its results, the teams that will compete in the “Final Eight” of the competition were determined.\n" +
                            "\n" +
                            "Canada, Great Britain, the Czech Republic and the Netherlands advanced to the playoffs as the winners of their quartets. Together with them in the quarterfinals will be the teams that took second place: Italy, Australia, Serbia and Finland.\n" +
                            "\n" +
                            "The Davis Cup playoffs will take place from November 21 to 26 in Malaga (Spain). It is noteworthy that the country where the final part of the tournament will be held was unable to get past the group round.\n" +
                            "\n" +
                            "Let us remind you that on ISPORT you can find the results of all matches of the group stage of the Davis Cup, as well as the composition of the teams at this stage.",
                    sevenImageId = R.drawable.sevensports_frid
                ),
                SevenSportsPost(
                    sevenTitle = "Davis Cup 2023: schedule and results",
                    sevenDate = sevenDateFormatted,
                    sevenInfo = "The Davis Cup is the largest international men's team tennis tournament. It has been held annually since 1900, and since 1979 under the auspices of the International Tennis Federation (ITF).\n" +
                            "\n" +
                            "In 2019, the usual format of the tournament changed. In September, immediately after the US Open, the group stage is held. The two best teams from each quartet advance to the Final Eight, which will be held in November.\n" +
                            "\n" +
                            "The status of the most titled team in the tournament belongs to the US team, which has won the competition 32 times. Next come Australia (28), France (10) and Great Britain (10).\n" +
                            "\n" +
                            "The current holder of the Davis Cup is the Canadian team, which last year beat the Australian team in the final (2:0).",
                    sevenImageId = R.drawable.sevensports_davis
                ),
                SevenSportsPost(
                    sevenTitle = "The former first racket of the world received a long",
                    sevenDate = sevenDateFormatted,
                    sevenInfo = "The International Tennis Integrity Agency (ITIA) announced on its official website that an independent tribunal has suspended Romanian tennis player Simona Halep for four years for violating the Tennis Anti-Doping Program (TADP).\n" +
                            "\n" +
                            "The two-time Grand Slam champion was charged with two separate TADP violations.\n" +
                            "\n" +
                            "The first involved an Adverse Analytical Finding (AAF) for the banned substance roxadustat at the 2022 US Open, conducted through routine in-competition urine testing.\n" +
                            "\n" +
                            "The second charge related to irregularities in Halep's biological passport (BPP).\u2028\u2028On Monday, 11 September 2023, the tribunal confirmed that the tennis player had committed a willful anti-doping rule violation under Article 2 of the TADP.\n" +
                            "\n" +
                            "The tribunal accepted Halep's argument that she took a contaminated supplement, but found that the volume she took could not have resulted in the concentration of roxadustat found in the positive sample.",
                    sevenImageId = R.drawable.sevensports_sim
                )
            )
        )
    }
}