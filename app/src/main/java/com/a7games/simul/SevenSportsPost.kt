package com.a7games.simul

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SevenSportsPost(
    val sevenTitle: String,
    val sevenDate: String,
    val sevenImageId: Int,
    val sevenInfo: String
) : Parcelable {
    fun mPost() = SevenSportsPost(
        sevenTitle,
        sevenDate,
        sevenImageId,
        sevenInfo
    )
}
