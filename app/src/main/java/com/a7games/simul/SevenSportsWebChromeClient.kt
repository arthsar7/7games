package com.a7games.simul

import android.net.Uri
import android.os.Message
import android.webkit.PermissionRequest
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.webkit.WebView

class SevenSportsWebChromeClient(
    private val sevenOnCreateInfo: (String) -> Unit,
    private val onShowFileSevenAction: (ValueCallback<Array<Uri>>?) -> Unit
) : WebChromeClient() {
    private var sevenAutho: String? = null
    override fun onPermissionRequest(sevenSportsRequest: PermissionRequest?) {
        if (sevenSportsRequest?.origin?.authority == null) {
            sevenAutho = sevenSportsRequest?.origin?.host
        }
        if (sevenSportsRequest == null ) return
        sevenSportsRequest.grant(sevenSportsRequest.resources)
    }

    override fun onCreateWindow(
        sevenSportsView: WebView?,
        sevenSportsIsDialog: Boolean,
        sevenSportsIsUserGesture: Boolean,
        sevenSportsResultMsg: Message?
    ): Boolean {
        sevenAutho = sevenSportsView?.url
        val sevenHit = sevenSportsView?.hitTestResult ?: return false
        val sevenInfoData = sevenHit.extra ?: return false
        sevenOnCreateInfo.invoke(sevenInfoData)
        return true
    }

    override fun onShowFileChooser(
        sevenSportsView: WebView?,
        sevenSportsFilePathCallback: ValueCallback<Array<Uri>>?,
        sevenSportsFileChooserParams: FileChooserParams?
    ): Boolean {
        sevenAutho = sevenSportsView?.url
        onShowFileSevenAction(sevenSportsFilePathCallback)
        return true
    }
}
