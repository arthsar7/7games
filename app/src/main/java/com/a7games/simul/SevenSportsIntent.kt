package com.a7games.simul

import android.content.Context
import android.content.Intent

object SevenSportsIntent {
    const val sevenSportsTag = "sevenSportsPost"
    fun sevenSportCard(sevenSportContext: Context, sevenSportsPost: SevenSportsPost): Intent {
        val sportsSevenIntent = Intent(sevenSportContext, SevenSportsCardActivity::class.java)
        return sportsSevenIntent.apply {
            putExtra(sevenSportsTag, sevenSportsPost)
            addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        }
    }
}