package com.a7games.simul

import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent


class SevenSportsCardActivity : ComponentActivity() {
    private val sportIntentData by lazy {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent?.getParcelableExtra(
                SevenSportsIntent.sevenSportsTag, SevenSportsPost::class.java
            )
        }
        else {
            intent?.getParcelableExtra(SevenSportsIntent.sevenSportsTag)
        } ?: SevenSportsInfoObj.sevenSportsPostList[0]
    }

    override fun onCreate(sevenSportsBundle: Bundle?) {
        SevenSportsObj.sevenInit(application)
        super.onCreate(sevenSportsBundle)
        setContent {
            SevenPostCard(
                sevenPost = sportIntentData,
                onSevenBackClick = {
                    finish()
                    overridePendingTransition(0,0)

                }
            )
        }
    }
}
