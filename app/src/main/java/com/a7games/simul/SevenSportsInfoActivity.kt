package com.a7games.simul

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent


class SevenSportsInfoActivity : ComponentActivity() {
    override fun onCreate(sevenSportsBundle: Bundle?) {
        super.onCreate(sevenSportsBundle)
        SevenSportsObj.sevenInit(application)
        setContent {
            SevenSportsInfo(
                onSevenBackClick = { finish() },
                onSevenPostClick = {
                    startActivity(
                        SevenSportsIntent.sevenSportCard(this, it)
                    )
                }
            )
        }
    }
}
